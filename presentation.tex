\documentclass[xcolor=svgnames,handout]{beamer}

\usepackage[utf8]    {inputenc}
\usepackage[T1]      {fontenc}
\usepackage[english] {babel}

\usepackage{amsmath,amsfonts,graphicx}
\usepackage{presentation}
\usepackage{bm}



\title
  [Computational Complexity - Assignment 1\hspace{2em}]
  {The polynomial hierarchy}

\author
  [Guilherme Rito]
  {Guilherme Rito\\ 51395}

\date
  {April 18, 2017}

\institute
  {FCT-UNL}


\begin{document}

\maketitle

\section
  {Introduction}

\begin{frame}
  {Guideline}

  \begin{enumerate}
  \item We consider \textbf{only} decision problems!
  \item The class $\bm{P}$\pause
  \item The class $\bm{NP}$\pause
  \item The class $\bm{coNP}$\pause
  \item The class $\bm{\Sigma_{2}^{p}}$
    \item The class $\bm{\Pi_{2}^{p}}$
  \item The Polynomial Hierarchy
  \end{enumerate}
\end{frame}



\section
  {Background}
  
\begin{frame}
  {The class $\bm{P}$}

  \begin{block}{Class $\bm{DTIME}$}
    Let $T: \mathbb{N} \to \mathbb{N}$ be some function.
    A language $L$ is in $\bm{DTIME}\left(T\left(n\right)\right)$ if and only if (iff) there is a (deterministic) Turing Machine (TM) that runs in time $c \cdot T\left(n\right)$ for some constant $c > 0$  and decides $L$.
  \end{block}
  

  Let $n$ denote the length of the input. Then,
  \[\bm{P} = \bigcup_{c \geq 1} \bm{DTIME\left(n^{c}\right)}\]

  Note that $\bm{P}$ is closed under complementation (\textit{i.e.} $\forall L: L \in \bm{P} \Leftrightarrow \overline{L} \in \bm{P}$).
  % For example, for L = {(x,y,z) s.t. x+y = z} in P then not L ({(x,y,z) s.t. x+y != z}) is also in P
\end{frame}




\begin{frame}
  {The class $\bm{NP}$}

  A language $L \subseteq \{0,1\}^{*}$ is in $\bm{NP}$ if there exists a polynomial $p: \mathbb{N} \to \mathbb{N}$ and a polynomial-time TM $M$ such that (s.t.) $\forall x \in \{0,1\}^{*}$
  
  \[x \in L \Leftrightarrow \exists u \in \{0,1\}^{p\left(\left | x \right |\right)} s.t. M\left(x,u\right) = 1\]

  \begin{itemize}
  \item We call $M$ a \textbf{verifier} for $L$.
  \item If $x \in L$, we call $u$ a \textbf{certificate} for $x$ (with respect to (w.r.t.) language $L$ and TM $M$).
  \item As we will see, it is not known whether $\bm{NP}$ is closed under complementation.
  \end{itemize}
\end{frame}




\begin{frame}
  {The class $\bm{NP}$}

  \begin{block}{Class $\bm{NTIME}$}
    For every function $T: \mathbb{N} \to \mathbb{N}$ and language $L \subseteq \{0,1\}^{*}$, we say that $L \in \bm{NTIME}\left(T\left(n\right)\right)$ if there is a constant $c > 0$ and a $c \cdot T\left(n\right)$-time Non-Deterministic Turing Machine (NDTM) s.t. $\forall x \in \{0,1\}^{*}$, $x \in L \Leftrightarrow M\left(x\right) = 1$.
  \end{block}
  
  \begin{theorem}
    $\bm{NP} = \cup_{c \in \mathbb{N}}\bm{NTIME}\left(n^{c}\right)$
  \end{theorem}
  % Proof idea? (Theorem 2.6, page 41/42)
\end{frame}



\begin{frame}
  {Reductions, $\bm{NP-hardness}$, $\bm{NP-completeness}$}

  A language $L \subseteq \{0,1\}^{*}$ is polynomial Karp reducible to a language ${L}' \subseteq \{0,1\}^{*}$ (denoted $L \leq_{p} {L}'$) if there is a polynomial-time computable function $f: \{0,1\}^{*} \to \{0,1\}^{*}$ s.t. $\forall x \in \{0,1\}^{*}$,
  \[x \in L \Leftrightarrow f\left(x\right) \in {L}'.\]

  \begin{theorem}
    If $L \leq_{p} {L}'$ and ${L}' \leq_{p} {L}''$ then $L \leq_{p} {L}''$.
  \end{theorem}
  % Proof idea? (Theorem 2.8, page 43)

  \begin{itemize}
  \item We say that a language ${L}'$ is $\bm{NP}$-hard if $\forall L \in \bm{NP}, L \leq_{p} {L}'$.
    \item We say that ${L}'$ is $\bm{NP}$-complete if ${L}'$ is $\bm{NP}$-hard and ${L}' \in \bm{NP}$.
  \end{itemize}
\end{frame}


\begin{frame}
  {Two $\bm{NP}$-complete languages}
  \begin{enumerate}
  \item Let $SAT = \left\{\varphi : \varphi \text{ is in CNF and } \exists assig \text{ s.t. } \varphi\left(assig\right) = true \right\}$.
  \begin{theorem}[Cook-Levin theorem]
    $SAT$ is $\bm{NP}$-complete.
  \end{theorem}
  % Proof idea? (Theorem 2.10, page 45-49)

\item Let $INDSET = \left\{ \left \langle G, k  \right \rangle : G \text{ has an independent set of size } k\right\}$, for a graph $G$.
  \begin{theorem}
    $INDSET$ is $\bm{NP}$-complete.
  \end{theorem}
  The proof of this theorem proves that $SAT \leq_{p} INDSET$, and hence that $INDSET$ is $\bm{NP}$-complete.
  % Proof idea? (Theorem 2.15, page 51/52)
  
  \end{enumerate}
  
\end{frame}


\begin{frame}
  {The class $\bm{coNP}$}

  The class $\bm{coNP}$ is defined as:
  \[\bm{coNP} = \left\{\overline{L}: L \in \bm{NP}\right\}\]

  Note that $\bm{coNP}$ is not the complement of $\bm{NP}$.
  
  In fact, not only $\bm{NP} \cap \bm{coNP} \neq \varnothing$, but also $\bm{P} \subseteq \bm{NP} \cap \bm{coNP}$.
\end{frame}


\begin{frame}
  {An alternative definition of the class $\bm{coNP}$}
  By $\bm{NP}$'s definition (and since $P$ is closed under complementation), we have:

  
  A language $L \subseteq \{0,1\}^{*}$ is in $\bm{coNP}$ if there exists a polynomial $p: \mathbb{N} \to \mathbb{N}$ and a polynomial-time TM $M$ s.t. $\forall x \in \{0,1\}^{*}$
  
  \[x \in L \Leftrightarrow \forall u \in \{0,1\}^{p\left(\left | x \right |\right)} \text{ we have }  M\left(x,u\right) = 1\]

  \begin{block}{Remark}
    The only difference between this alternative definition of $\bm{coNP}$ and the definition of $\bm{NP}$ is the quantifier (for $\bm{coNP}$ it is $\forall$ while for $\bm{NP}$ it is $\exists$).
  \end{block}
\end{frame}


\begin{frame}
  {Two $\bm{coNP}$-complete languages}
  \begin{enumerate}
  \item Let $TAUTOLOGY = \left\{\varphi : \varphi \text{ is in CNF and } \forall assig \, \varphi\left(assig\right) = true \right\}$.
  \begin{theorem}
    $TAUTOLOGY$ is $\bm{coNP}$-complete.
  \end{theorem}
  % Proof idea? (Example 2.21, page 56) - Modify the proof of Cook-Levin.

\item Let $EQ$-$DNF = \left\{ \left \langle \varphi, \psi  \right \rangle : \forall assig \, \varphi\left(assig\right) = \psi\left(assig\right)\right\}$, where $\varphi, \psi$ are boolean formulas in DNF.
  \begin{theorem}
    $EQ$-$DNF$ is $\bm{coNP}$-complete.
  \end{theorem}
  % Proof idea? (damn it was my idea)
  \end{enumerate}
  
\end{frame}



\begin{frame}
  {$\bm{NP} \overset{?}{=} \bm{coNP}$}

  \begin{itemize}
  \item  Although it is unknown, most researchers believe that $\bm{NP} \neq \bm{coNP}$.
    \begin{block}{Reason}
      It does not seem possible to have a certificate with polynomial length may guarantee that some formula $\varphi$ is a tautology (\textit{i.e.} that $\varphi \in TAUTOLOGY$).
    \end{block}

  \item At an intuitive level, it seems that to guarantee $\varphi$ is a tautology, we have to try all possible variable assignments, and only then we may conclude that $\varphi \in TAUTOLOGY$.
    \item Note that this is the exact opposite as verifying if a formula is satisfiable (in this case, if $\neg\varphi \in SAT$).
  \end{itemize}
\end{frame}


\section{The Polynomial Hierarchy}


\begin{frame}
  {The class $\bm{\Sigma_{2}^{p}}$}

  The class $\bm{\Sigma_{2}^{p}}$ is the set of all languages $L$ for which there exists a polynomial-time TM $M$ and a polynomial q s.t. \[x \in L \Leftrightarrow \exists u \in \left\{0,1\right\}^{q\left(\left | x \right |\right)} \forall v \in \left\{0,1\right\}^{q\left(\left | x \right |\right)} M\left(x,u,v\right) = 1 \]
  $\forall x \in \left\{0,1\right\}^{*}$
\end{frame}



\begin{frame}
  {A language in $\bm{\Sigma_{2}^{p}}$}
  Recall that $INDSET = \left\{ \left \langle G, k  \right \rangle : G \text{ has an independent set of size } k\right\}$

  Let $EXACT$-$INDSET$ be the language $\left\{ \left \langle G, k  \right \rangle : \text{The largest independent set in } G \text{ has size } k\right\}$.
  \begin{theorem}
    $EXACT$-$INDSET \in \bm{\Sigma_{2}^{p}}$.
  \end{theorem}
\end{frame}


\begin{frame}
  {A $\bm{\Sigma_{2}^{p}}$-complete language}
  
  Recall that $EQ$-$DNF = \left\{ \left \langle \varphi, \psi  \right \rangle : \forall assig \, \varphi\left(assig\right) = \psi\left(assig\right)\right\}$, where $\varphi, \psi$ are boolean formulas in DNF ($EQ$-$DNF \in \bm{coNP}$).
  
  Let $MIN$-$EQ$-$DNF$ be $\left\{ \left \langle \varphi, k  \right \rangle : \exists \psi \text{ s.t. } \left | \psi \right | \leq k \wedge \forall assig \, \varphi \left(assig\right) = \psi \left(assig\right)\right\}$, where $\varphi, \psi$ are boolean formulas in DNF.
  \begin{theorem}
    $MIN$-$EQ$-$DNF$ is $\bm{\Sigma_{2}^{p}}$-complete.
  \end{theorem}
\end{frame}










\begin{frame}
  {The class $\bm{\Pi_{2}^{p}}$}

  The class $\bm{\Pi_{2}^{p}}$ is the set of all languages $L$ for which there exists a polynomial-time TM $M$ and a polynomial q s.t. \[x \in L \Leftrightarrow \forall u \in \left\{0,1\right\}^{q\left(\left | x \right |\right)} \exists v \in \left\{0,1\right\}^{q\left(\left | x \right |\right)} M\left(x,u,v\right) = 1 \]
  $\forall x \in \left\{0,1\right\}^{*}$
\end{frame}



\begin{frame}
  {A $\bm{\Pi_{2}^{p}}$-complete language}
  
  Let $\overline{MIN\text{-}EQ\text{-}DNF}$ be $\left\{ \left \langle \varphi, k  \right \rangle : \forall \psi \text{ s.t. } \left | \psi \right | \leq k \, \exists assig \, \varphi \left(assig\right) \neq \psi \left(assig\right)\right\}$, where $\varphi, \psi$ are boolean formulas in DNF.
  \begin{theorem}
    $\overline{MIN\text{-}EQ\text{-}DNF}$ is $\bm{\Pi_{2}^{p}}$-complete.
  \end{theorem}
\end{frame}






\begin{frame}
  {The Polynomial Hierarchy}

  The polynomial hierarchy generalizes the definition of $\bm{NP}$, $\bm{coNP}$, $\bm{\Sigma_{2}^{p}}$ and $\bm{\Pi_{2}^{p}}$.
\end{frame}

\begin{frame}
  {The Polynomial Hierarchy}

  For $i \geq 1$, a language $L$ is in $\bm{\Sigma_{i}^{p}}$ if there exists a TM $M$ and a polynomial q s.t.
  \begin{align*}
  x \in L \Leftrightarrow
    \exists u_{1} &\in \left \{0,1 \right\}^{q\left(\left | x \right |\right)}
    \forall u_{2} \in \left \{0,1 \right\}^{q\left(\left | x \right |\right)}
    \ldots
    Q_{i}   u_{i} \in \left \{0,1 \right\}^{q\left(\left | x \right |\right)}\\
    &M\left(x, u_{1}, u_{2}, \ldots, u_{i}\right) = 1
  \end{align*}
  where $Q_{i}$ is either $\forall$ (if $i$ is even) or $\exists$ (otherwise).

  The \textbf{Polynomial Hierarchy} is defined as $\bm{PH} = \cup_{i \geq 1} \bm{\Sigma_{i}^{p}}$.
\end{frame}


\begin{frame}
  {The Polynomial Hierarchy}

  For $i \geq 1$, a language $L$ is in $\bm{\Sigma_{i}^{p}}$ if there exists a TM $M$ and a polynomial q s.t.
  \begin{align*}
  x \in L \Leftrightarrow
    \exists u_{1} &\in \left \{0,1 \right\}^{q\left(\left | x \right |\right)}
    \forall u_{2} \in \left \{0,1 \right\}^{q\left(\left | x \right |\right)}
    \ldots
    Q_{i}   u_{i} \in \left \{0,1 \right\}^{q\left(\left | x \right |\right)}\\
    &M\left(x, u_{1}, u_{2}, \ldots, u_{i}\right) = 1
  \end{align*}
  where $Q_{i}$ is either $\forall$ (if $i$ is even) or $\exists$ (otherwise).

  The \textbf{Polynomial Hierarchy} is defined as $\bm{PH} = \cup_{i \geq 1} \bm{\Sigma_{i}^{p}}$.

  \begin{block}{$\bm{\Pi_{i}^{p}}$}
    We define $\bm{\Pi_{i}^{p}}$ as $\bm{\Pi_{i}^{p}} = \bm{co\Sigma_{i}^{p}} = \left\{\overline{L}: L \in \bm{\Sigma_{i}^{p}}\right\}$
\end{block}

\end{frame}


\begin{frame}
  {The Polynomial Hierarchy}

    Note that:
  \begin{itemize}
  \item $\bm{\Sigma_{1}^{p}} = \bm{NP}$
  \item $\bm{\Pi_{1}^{p}} = \bm{coNP}$
  \end{itemize}
\end{frame}

\begin{frame}
  {The Polynomial Hierarchy}

  We define $\bm{\Pi_{i}^{p}}$ as $\bm{\Pi_{i}^{p}} = \bm{co\Sigma_{i}^{p}} = \left\{\overline{L}: L \in \bm{\Sigma_{i}^{p}}\right\}$

  Note that:
  \begin{itemize}
  \item $\bm{\Sigma_{1}^{p}} = \bm{NP}$
  \item $\bm{\Pi_{1}^{p}} = \bm{coNP}$
  \item $\bm{\Sigma_{i}^{p}} \subseteq \bm{\Pi_{i+1}^{p}} \subseteq \bm{\Sigma_{i+2}^{p}}  \subseteq \ldots$
  \end{itemize}
\end{frame}

\begin{frame}
  {The Polynomial Hierarchy}

  We define $\bm{\Pi_{i}^{p}}$ as $\bm{\Pi_{i}^{p}} = \bm{co\Sigma_{i}^{p}} = \left\{\overline{L}: L \in \bm{\Sigma_{i}^{p}}\right\}$

  Note that:
  \begin{itemize}
  \item $\bm{\Sigma_{1}^{p}} = \bm{NP}$
  \item $\bm{\Pi_{1}^{p}} = \bm{coNP}$
  \item $\bm{\Sigma_{i}^{p}} \subseteq \bm{\Pi_{i+1}^{p}} \subseteq \bm{\Sigma_{i+2}^{p}}  \subseteq \ldots$
  \item $\bm{PH} = \cup_{i \geq 1} \bm{\Pi_{i}^{p}}$
  \end{itemize}
\end{frame}



\begin{frame}
  {The Polynomial Hierarchy}

  We say that ``The Polynomial Hierarchy \textit{collapses}'' if $\exists i \text{ s.t. } \bm{\Sigma_{i}^{p}} = \bm{\Sigma_{i+1}^{p}}$.
  
  In fact, as we will see, for such $i$, $\bm{\Sigma_{i}^{p}} = \bm{PH}$.
\end{frame}


\begin{frame}
  {$\bm{P} = \bm{NP} \Rightarrow \bm{P} = \bm{PH}$}

  We prove this claim by induction.

  Base case:
  $\bm{P} = \bm{NP}$ then $\bm{P} = \bm{NP} = \bm{coNP}$

  Step:
  On the board.

\end{frame}

\begin{frame}
  {$\bm{\Sigma_{i}^{p}} = \bm{\Pi_{i}^{p}} \Rightarrow \bm{\Sigma_{i}^{p}} = \bm{PH}$}

  The proof of this claim is identical to the previous.
\end{frame}



\begin{frame}
  {If $\exists L \text{ s.t. } L$ is $\bm{PH}$-complete, then the hierarchy collapses}

  \begin{block}{Proof Idea}
    \begin{enumerate}
    \item By definition $\bm{PH} = \cup_{i \geq 1} \bm{\Sigma_{i}^{p}}$.
    \item Since $L \in \bm{PH}$, $\exists i \text{ s.t. } L \in \bm{\Sigma_{i}^{p}}$.
    \item Since $L$ is $\bm{PH}$-complete, we can reduce all languages of $\bm{PH}$ to $L$.
    \item However, every language that is $\leq_{p}$ to a language in $\bm{\Sigma_{i}^{p}}$ is itself in $\bm{\Sigma_{i}^{p}}$, implying $\bm{PH} \subseteq \bm{\Sigma_{i}^{p}}$.
    \item Thus, $\bm{PH} = \bm{\Sigma_{i}^{p}}$
    \end{enumerate}
  \end{block}

%  \begin{exampleblock}{Corollary}
%  $\bm{PH} \subseteq \bm{PSPACE}$
%  \end{exampleblock}

  
\end{frame}

\begin{frame}
  {Complete problems for each level.}

  $\forall i \geq 1$, consider the following complete problem (a variant of $SAT$, using $i$ quantifiers):
  \[\bm{\Sigma_{i}^{p}}SAT = \exists u_{1} \forall u_{2} \ldots Q_{i}u_{i} \varphi\left(u_{1},u_{2},\ldots,u_{i}\right) = 1\]
  where $\varphi$ is a boolean formula, each $u_{i}$ a boolean vector, and $Q_{i}$ either $\forall$ or $\exists$, depending on whether (respectively) $i$ is even or odd.
  
\end{frame}

\begin{frame}
  {Complete problems for each level.}

  $\forall i \geq 1$, consider the following complete problem (a variant of $SAT$, using $i$ quantifiers):
  \[\bm{\Sigma_{i}^{p}}SAT = \exists u_{1} \forall u_{2} \ldots Q_{i}u_{i} \varphi\left(u_{1},u_{2},\ldots,u_{i}\right) = 1\]
  where $\varphi$ is a boolean formula, each $u_{i}$ a boolean vector, and $Q_{i}$ either $\forall$ or $\exists$, depending on whether (respectively) $i$ is even or odd.

  \begin{block}{Theorem}
    $\bm{\Sigma_{i}^{p}}SAT$ is $\bm{\Sigma_{i}^{p}}$-complete. 
  \end{block}
  % The proof of this theorem is based on the fact that SAT is NP-complete (or $\bm{\Sigma_{1}^{p}}$-complete). % (Exercise 5.1, page 104)
  
\end{frame}

\begin{frame}
  {Complete problems for each level.}

  It follows:

  \begin{block}{Corollary}
    $\overline{\bm{\Sigma_{i}^{p}}SAT}$ is $\bm{\Pi_{i}^{p}}$-complete. 
  \end{block}

  Note that this language is analogous to the $TAUTOLOGY$ language (by the definition of $\bm{\Pi_{i}^{p}}$).
  
\end{frame}

\begin{frame}
  {Alternating Turing Machines}

  The notion of Alternating Turing Machines (ATM) is a generalization of NDTMs.

  For an ATM $M$, each state $q \in Q$ (where $Q$ is the set of states of $M$), is labelled as either an $accepting$, $rejecting$, $\exists$ or $\forall$ state.

  A NDTM can be seen as an ATM whose all non accepting nor rejecting states are labelled as $\exists$.
  
\end{frame}

\begin{frame}
  {The class $\bm{ATIME}$}
  
    For every function $T: \mathbb{N} \to \mathbb{N}$, we say that an ATM $M$ runs in $T\left(n\right)$-time if $\forall x \in \{0,1\}^{*}$ and for every possible sequence of transition function choices, $M$ halts within $T\left(\left | n \right |\right)$ steps.

    We say that a language $L \subseteq \{0,1\}^{*}$, $L \in \bm{ATIME}\left(T\left(n\right)\right)$ if there is a constant $c > 0$ and a $c \cdot T\left(n\right)$-time ATM $M$ s.t.
    $\forall x \in \{0,1\}^{*}$, $M$ accepts $x$ iff $x \in L$.

    
    Regarding when an ATM accepts some input, recall last lecture :)
    
  
  
  
\end{frame}


\begin{frame}
  {The classes $\bm{\Sigma_{i}^{p}TIME}$ and $\bm{\Pi_{i}^{p}TIME}$}
  
  $\forall i \in \mathbb{N}$, we define $\bm{\Sigma_{i}^{p}TIME}\left(T\left(n\right)\right)$ to be the set of languages accepted by a $T\left(n\right)$-time ATM $M$ whose initial state is labelled $\exists$ and on which every input and on every (directed) path from the starting configuration in the configuration graph, $M$ can alternate at most $i-1$ times from states with one label to states with the other label.

  \begin{block}{Theorem}
    $\forall i \in \mathbb{N}, \, \bm{\Sigma_{i}^{p}} = \cup_{c}\bm{\Sigma_{i}^{p}TIME}\left(n^{c}\right)$
  \end{block}
  % elaborate on this
\end{frame}




\begin{frame}
  {The classes $\bm{\Sigma_{i}^{p}TIME}$ and $\bm{\Pi_{i}^{p}TIME}$}
  
  $\forall i \in \mathbb{N}$, we define $\bm{\Pi_{i}^{p}TIME}\left(T\left(n\right)\right)$ to be the set of languages accepted by a $T\left(n\right)$-time ATM $M$ whose initial state is labelled $\forall$ and on which every input and on every (directed) path from the starting configuration in the configuration graph, $M$ can alternate at most $i-1$ times from states with one label to states with the other label.

  \begin{block}{Theorem}
    $\forall i \in \mathbb{N}, \, \bm{\Pi_{i}^{p}} = \cup_{c}\bm{\Pi_{i}^{p}TIME}\left(n^{c}\right)$
  \end{block}
  % elaborate on this
\end{frame}



\begin{frame}
  {Turing Machines with an Oracle}

  \begin{itemize}
  \item  A Turing Machine with an oracle for a language $O$, is a Machine that can ask its oracle if some string $q$ is in $O$.
  \item To do so, the machine has a special tape, the \textit{oracle tape} on which it writes the string $q$.
  \item  TMs with an oracle have three additional states, used for interacting with the oracle: $q_{query}, q_{yes}, q_{no}$.
  \item To ask an oracle if $q \in O$, the machine writes $q$ on the oracle tape, and then changes to the state $q_{query}$.
  \item In the next step, the oracle ``magically'' responds to the query, going to either $q_{yes}$ (meaning $q \in O$ or $q_{no}$ meaning $q \notin O$.
  \end{itemize}

  The decision of a TM $M$ that has access to an oracle for deciding some language $L$, for some input $x$ is denoted by $M^{L}\left(x\right)$.
  
\end{frame}

\begin{frame}
  {Turing Machines with an Oracle}

  For every $O \subseteq \left\{0,1\right\}^{*}$, $\bm{P}^{O}$ is the set containing every language that can be decided by a polynomial-time TM with oracle access to $O$.

  For every $O \subseteq \left\{0,1\right\}^{*}$, $\bm{NP}^{O}$ is the set containing every language that can be decided by a polynomial-time NDTM with oracle access to $O$.
  
  Often, when $L$ is complete for some complexity class $\Gamma$, we can also write $M^{\Gamma}$, since $\forall {L}' \in \Gamma, \, {L}' \leq_{p} L$.
  
\end{frame}


\begin{frame}
  {Turing Machines with an Oracle}

  Note that:
  \begin{itemize}
  \item Consider $\overline{SAT}$.
    It is easy to see that $\overline{SAT} \in \bm{P}^{SAT}$

  \item Let $O \in \bm{P}$.
    Then, it is also easy to see that $\bm{P}^{O} =  \bm{P}$.

    % example of counting?
  \end{itemize}
\end{frame}




\begin{frame}
  {Turing Machines with an Oracle}

  This notion is so powerful that we can even define TMs with access to oracles that answer if some $\alpha$ (\textit{i.e.} a machine encoded by $\alpha$) will halt (for some input), \textbf{in a single step!}.
  \textit{i.e.} we could define a TM $M^{HALT}$ and use it.
  
\end{frame}



\begin{frame}
  {Defining the hierarchy using TMs with oracles}

  \begin{theorem}
    $\forall i \geq 2, \bm{\Sigma_{i}^{p}} = \bm{NP}^{\bm{\Sigma_{i-1}} SAT}$, where $\bm{NP}^{\bm{\Sigma_{i-1}} SAT}$ denotes the set of languages decided by a polynomial-time TM $M$ with access to the oracle $\bm{\Sigma_{i-1}} SAT$.
  \end{theorem}

  We give a proof idea for a particular example $\bm{\Sigma_{2}^{p}} = \bm{NP}^{SAT}$.
\end{frame}



\begin{frame}
  {$\Leftarrow$}

  \begin{lemma}
    $\bm{\Sigma_{2}^{p}} \subseteq \bm{NP}^{SAT}$
  \end{lemma} 
\end{frame}

\begin{frame}
  {$\Rightarrow$}

  \begin{lemma}
    $\bm{NP}^{SAT} \subseteq \bm{\Sigma_{2}^{p}} $
  \end{lemma}
\end{frame}

\section{Conclusion}

\begin{frame}
  {Thank you! :)}

\end{frame}
\end{document}

