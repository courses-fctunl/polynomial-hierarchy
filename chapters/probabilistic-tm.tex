\begin{definition}[Probabilistic Turing Machine]
  \label{def:probabilistic-turing-machine}
  A single-taped \emph{Probabilistic Turing Machine} (PTM) is a tuple \[M = \left \langle Q, \Sigma, \Gamma, \delta_0, \delta_1, q_{0}, q_{accept}, q_{reject} \right \rangle,\] where all $Q$, $\Sigma$ and $\Gamma$ are all finite sets and
  \begin{enumerate}
  \item $Q$ is the set of states.
  \item $\Sigma$ is the input alphabet, not containing the \emph{blank symbol} \textvisiblespace ,
  \item $\Gamma$ is the tape alphabet, where $\textvisiblespace \in \Gamma$ and $\Sigma \subseteq \Gamma$,
  \item for $b \in \braces{0,1}, \delta_b: Q \times \Gamma \rightarrow Q \times \Gamma \times \left\{L,R,S\right\}$ are the transition functions,
  \item $q_{0} \in Q$ is the start state,
  \item $q_{accept} \in Q$ is the accept state, and
  \item $q_{reject} \in Q$ is the reject state (where $q_{accept} \neq q_{reject}$).
  \end{enumerate}
\end{definition}

A PTM $M$ is a TM with two transition functions $\delta_0$ and $\delta_1$; to
execute a PTM, at each step one picks a bit $b$ (uniformly at random, and
independently for each step) and then executes transition function $\delta_b$.
%
For an input $x$, $M(x)$ denotes the random variable (with domain
$\braces{q_{accept},q_{reject}}$ induced by running PTM $M$ on input $x$.
%
To denote the execution of $M$ on input $x$ using a bitstring $\vec{r}$ as the
sequence of random bits we write $M(x;\vec{r})$.


\begin{definition}[The class $\bm{BPTIME}$]
  \label{def:bptime}
  Let $T: \mathbb{N} \rightarrow \mathbb{N}$ be some function and $L \subseteq \boolsset^*$ be some language.
  We say $L$ is in $\bm{BPTIME}\left(T\left(n\right)\right)$ iff there is a PTM
  $M$ that runs in time $T\left(\abs{x}\right)$ and satisfies
  \[\mprob{M(x) = q_{accept} \Leftrightarrow x \in L} \geq 2/3.\]
\end{definition}


\begin{definition}[The class $\bm{BPP}$]
  \label{def:class-BPP}
  \[\bm{BPP} = \bigcup_{c \geq 1}\bm{BPTIME}\left(n^{c}\right).\]
\end{definition}


\begin{definition}[The class $\bm{RPTIME}$]
  \label{def:rptime}
  Let $T: \mathbb{N} \rightarrow \mathbb{N}$ be some function and $L \subseteq \boolsset^*$ be some language.
  We say $L$ is in $\bm{RPTIME}\left(T\left(n\right)\right)$ iff there is a PTM
  $M$ that runs in time $T\left(\abs{x}\right)$ and satisfies
  \begin{enumerate}
  \item $\mprob{x \in L \Rightarrow M(x) = q_{accept}} \geq 1/2$;
  \item $\mprob{x \not\in L \Rightarrow M(x) = q_{accept}} = 0$.
  \end{enumerate}
\end{definition}

\begin{definition}[The class $\bm{RP}$]
  \label{def:class-RP}
  \[\bm{RP} = \bigcup_{c \geq 1}\bm{RPTIME}\left(n^{c}\right).\]
\end{definition}

\begin{definition}[The class $\bm{co}\text{-}\bm{RPTIME}$]
  \label{def:co-rptime}
  Let $T: \mathbb{N} \rightarrow \mathbb{N}$ be some function and $L \subseteq \boolsset^*$ be some language.
  We say $L$ is in $\bm{co}\text{-}\bm{RPTIME}\left(T\left(n\right)\right)$ iff there is a PTM
  $M$ that runs in time $T\left(\abs{x}\right)$ and satisfies
  \begin{enumerate}
  \item $\mprob{x \in L \Rightarrow M(x) = q_{accept}} = 1$;
  \item $\mprob{x \not\in L \Rightarrow M(x) = q_{accept}} \leq 1/2$.
  \end{enumerate}
\end{definition}

\begin{definition}[The class $\bm{co}\text{-}\bm{RP}$]
  \label{def:class-co-RP}
  \[\bm{co}\text{-}\bm{RP} = \bigcup_{c \geq 1}\bm{co}\text{-}\bm{RP}\left(n^{c}\right).\]
\end{definition}

\begin{definition}[The class $\bm{ZPTIME}$]
  \label{def:zptime}
  Let $T: \mathbb{N} \rightarrow \mathbb{N}$ be some function.
  We say $L$ is in $\bm{ZPTIME}\left(T\left(n\right)\right)$ iff there is a PTM
  $M$ that in expectation runs in time $T\left(\abs{x}\right)$ and decides $L$.
\end{definition}

\begin{definition}[The class $\bm{ZPP}$]
  \label{def:class-ZP}
  \[\bm{ZPP} = \bigcup_{c \geq 1}\bm{ZPTIME}\left(n^{c}\right).\]
\end{definition}

\begin{theorem}
  \label{thr:zpp-is-rp-cap-co-rp}
  $\bm{ZPP} = \bm{RP} \cap \bm{co}\text{-}\bm{RP}$.
\end{theorem}

\begin{theorem}
  \label{thr:basic-probabilistic-class-relations}
  \hfill
  \begin{enumerate}
  \item $\bm{RP} \subseteq \bm{BPP}$;
  \item $\bm{co}\text{-}\bm{RP} \subseteq \bm{BPP}$.
  \end{enumerate}
\end{theorem}

\begin{definition}[The class $\bm{PTIME}$]
  \label{def:ptime}
  Let $T: \mathbb{N} \rightarrow \mathbb{N}$ be some function and $L \subseteq \boolsset^*$ be some language.
  We say $L$ is in $\bm{PTIME}\left(T\left(n\right)\right)$ iff there is a PTM
  $M$ that runs in time $T\left(\abs{x}\right)$ and satisfies
  \[\mprob{M(x) = q_{accept} \Leftrightarrow x \in L} > 1/2.\]
\end{definition}


\begin{definition}[The class $\bm{PP}$]
  \label{def:class-PP}
  \[\bm{PP} = \bigcup_{c \geq 1}\bm{PTIME}\left(n^{c}\right).\]
\end{definition}


\begin{table}[h!]
  \begin{tabular}{| c || c | c | c |}\hline
    \multirow{2}{*}{Compl. Class} & Prob. Accept & Prob. Reject    & \multirow{2}{*}{Runtime} \\
                                  & ($x \in L$)  & ($x \not\in L$) & \\ \hline
    $\bm{ZPP}$                    & $1$          & $1$             & $\mexpval{\text{poly}(\cdot)}$ \\ \hline
    $\bm{RP}$                     & $\geq 1/2$   & $1$             & $\text{poly}(\cdot)$ \\ \hline
    $\bm{co}\text{-}\bm{RP}$      & $1$          & $\geq 1/2$      & $\text{poly}(\cdot)$ \\ \hline
    $\bm{BPP}$                    & $\geq 2/3$   & $\geq 2/3$      & $\text{poly}(\cdot)$ \\ \hline
    $\bm{PP}$                     & $> 1/2$      & $< 1/2$         & $\text{poly}(\cdot)$\\ \hline
  \end{tabular}
  \caption{In the table we consider a fixed language $L \subseteq \bools^*$.}
  \label{table:probabilistic-complexity-classes-with-error-summary}
\end{table}



%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../report"
%%% End:
