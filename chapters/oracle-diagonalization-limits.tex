The diagonalization arguments used for the time and space hierarchy theorems
relied on two properties of \tm s:
\begin{itemize}
\item any given \tm\ (or \ndtm) can be represented by a string;
\item one can efficiently simulate a \tm\ given its string representation (via a
  \utm\ or \undtm).
\end{itemize}
%
This means that the arguments only accessed these machines in a black-box
manner---and in particular they did not depend on the internal workings of any
machine.
%
We can define a \tm\ with access to an oracle for a language
$L\subseteq\bitstrings$ as a \tm\ (see~\Cref{def:turing-machine}) with an
additional oracle tape and three additional states: $q_{\text{query}}$,
$q_{\text{yes}}$, $q_{\text{no}}$.
%
To query the oracle, the machine first writes some string $s$ (possibly the
empty string) into its oracle tape, and then enters state $q_{\text{query}}$: if
$s \in L$ then the machine transitions to state $q_{\text{yes}}$, and otherwise
it transitions to state $q_{\text{no}}$.
%


\begin{definition}[$\oracles{\CompClass{DTIME}}{O}$]
  \label{def:dtime-oracle}
  Let $\fndef{T}{\nats}{\nats}$ be some function.
  A language $L$ is in $\fn{\oracles{\CompClass{DTIME}}{O}}{\fn{T}{n}}$ \miff\
  there is a \tm\ $M$ that, given access to an oracle for a language $O
  \subseteq \bitstrings$, $M$ runs in time $c \cdot \fn{T}{n}$ for some constant
  $c > 0$ and decides $L$.
\end{definition}


\begin{definition}[$\oracles{\ClassP}{O}$]
  \label{def:class-P-oracle}
  \[\oracles{\ClassP}{O} = \bigcup_{c \geq 1}\fn{\oracles{\CompClass{DTIME}}{O}}{n^{c}}.\]
\end{definition}


\begin{definition}[$\oracles{\CompClass{NTIME}}{O}$]
  \label{def:ntime-oracle}
  Let $\fndef{T}{\nats}{\nats}$ be some function.
  A language $L$ is in $\fn{\oracles{\CompClass{NTIME}}{O}}{\fn{T}{n}}$ \miff\
  there is a \ndtm\ $M$ that, given access to an oracle for a language $O
  \subseteq \bitstrings$, $M$ runs in time $c \cdot \fn{T}{n}$ for some constant
  $c > 0$ and decides $L$.
\end{definition}


\begin{definition}[$\oracles{\ClassNP}{O}$]
  \label{def:class-NP-oracle}
  \[\oracles{\ClassNP}{O} = \bigcup_{c \geq 1}\fn{\oracles{\CompClass{NTIME}}{O}}{n^{c}}.\]
\end{definition}



\begin{remark}
  \label{remark:equivalence-oracle-machines}
  Two remarks about oracle complexity classes.
  \begin{enumerate}
  \item Let $O \in \ClassP$.
    Then, it is also easy to see that $\ClassP^{O} =  \ClassP$.

  \item Consider the language $\overline{\Lang{SAT}}$.
    It is easy to see that $\overline{\Lang{SAT}} \in \ClassP^{\Lang{SAT}}$.
  \end{enumerate}
\end{remark}

For a language $L$ that is complete for a complexity class \CompClass{C}, one
can alternatively write $\oracles{M}{\CompClass{C}}$, instead of writing
$\oracles{M}{L}$.

Consider the following language:
\[\Lang{EXPCOM} = \braces{\parens{M,x,1^n}: M \text{ outputs } 1 \text{ on } x
    \text{ within } 2^n \text{ steps }}\]

\begin{proposition}
  \label{prop:p-np-exp-expcom-equal}
  $\oracles{\ClassP}{\Lang{EXPCOM}} = \oracles{\ClassNP}{\Lang{EXPCOM}} = \ClassEXP$.
\end{proposition}
\begin{iproof}
  \begin{description}
  \item[$\ClassEXP \subseteq \oracles{\ClassP}{\Lang{EXPCOM}}$] Note that a \tm\
    can use the oracle to perform an exponential amount of computation (by
    giving an adequate \tm\ as input to the \Lang{EXPCOM} oracle.
  \item[$\oracles{\ClassP}{\Lang{EXPCOM}} \subseteq
    \oracles{\ClassNP}{\Lang{EXPCOM}}$] This is trivial.
  \item[$\oracles{\ClassNP}{\Lang{EXPCOM}} \subseteq \ClassEXP$] A \tm\ can
    guess all the (non-deterministic) choices of an \ndtm---there are up to
    $2^l$ possibilities, if the runtime of the \ndtm\ is $l$---and answer all
    its oracle queries by computing the language of the oracle itself (which it
    can do in exponential time)---each query requires running a \tm\ for at most
     $2^l$ steps, where $1^l$ is part of the input to the oracle. (Informal)
     Note there are at most $l$ non-deterministic choices, so there are at most
     $2^l$ queries to the oracle, and each query requires running a \tm\ for at
     most $2^l$ steps (since the unary part of the input is of length at most
     $l$), so altogether it takes at most $2^l \cdot 2^l = 2^{2\cdot l}$ steps.
  \end{description}
\end{iproof}


\begin{theorem}
  \label{thr:baker-gill-solovay}
  There are oracles $A$ and $B$ such that: $\oracles{\ClassP}{A} =
  \oracles{\ClassNP}{A}$ and $\oracles{\ClassP}{B} \neq \oracles{\ClassNP}{B}$.
\end{theorem}
\begin{iproof}
  For oracle $A$, one can consider language $\Lang{EXPCOM}$ from above.

  
  For any language $B$, define unary language $U_B$ as:
  \[U_B = \braces{1^n: B \cap \strings{\bools}{n} \neq \emptyset}.\]

  
  First, note that any \ndtm\ $M$ with oracle access to $B$ that runs in
  polynomial time can decide $U_B$:
  on input $1^n$, \ndtm\ $M$ can use its non-determinism to guess a bitstring of
  length $n$ that is in $B$.
  %
  Therefore, $U_B \in \oracles{\ClassNP}{B}$.
  %
  We now construct a language $B$ such that $U_B \notin \oracles{\ClassP}{B}$.

  We construct language $B$ in stages: initially $B$ is empty, and at each stage
  we decide on the membership (with respect to $B$) of a finite number of
  strings.
  %
  At a high level, each stage $i \in \nats$ ensures
  $\oracles{\tmofstring{i}}{B}$ does not decide $U_B$ in $\frac{2^n}{10}$ time.

  \textbf{Stage $i$:} Pick $n \in \nats$ so that $n$ is larger than any string
  $x$ whose membership in $B$ was already decided (in a previous stage).
  %
  Run $\fn{\tmofstring{i}}{1^n}$ for $\frac{2^n}{10}$ steps; for each oracle
  query on an input $x$ made by \tmofstring{i}, if the membership of $x$ in $B$
  had been decided already, answer accordingly, and otherwise define $x \notin
  B$.
  %
  \begin{description}
  \item[\tmofstring{i} accepts] define $B$ not to include any string of length
    $n$, which implies $1^n \notin U_B$; this is possible because $n$ is such
    that there is no string of length $n$ whose membership in $B$ has already
    been decided.
  \item[\tmofstring{i} rejects] define $B$ to include some string of length $n$,
    which \tmofstring{i} has not queried to $B$---such a string exists because
    \tmofstring{i} was only run for $\frac{2^n}{10}$ steps, and there are $2^n >
    \frac{2^n}{10}$ strings of length $n$ (for large enough $n$).
  \end{description}
  Since every \tm\ has a finite bitstring representation, the definition of
  language $B$ implies no \tm\ decides $U_B$.
  
  The way $B$ is defined actually implies that $U_B \notin
  \oracles{\fn{\CompClass{DTIME}}{\fn{f}{n}}}{B}$, for any $\fn{f}{n} =
  \littleo{2^n}$.
\end{iproof}



%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../report"
%%% End:
