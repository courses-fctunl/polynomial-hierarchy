\begin{definition}[$\ClassNP$]
  \label{def:class-NP}
  A language $L \subseteq \bitstrings$ is in $\ClassNP$ if there is a polynomial $\fndef{p}{\nats}{\nats}$ and a (\Pt) \pt\ $M$ s.t. $\forall x \in \bitstrings$, 
  \[x \in L \qquad \Leftrightarrow \qquad \exists u \in \strings{\bools}{\fn{p}{\length{x}}} \, \mst\ \, \fn{M}{x,u} = 1.\]
  If $x \in L$ and $u \in \strings{\bools}{\fn{p}{\length{x}}}$ satisfy $\fn{M}{x,u} = 1$, then we call $u$ a \emph{certificate} (or \emph{witness}) for $x$ (w.r.t. language $L$ and machine $M$).
\end{definition}

Contrarily to $\ClassP$, it is not known whether $\ClassNP$ is closed under
complementation (i.e. if $\ClassNP = \ClassCoNP$).


The definition of many of the variants of the \Tm\ only differ on the transition
function. For instance, a \Ndtm\ (\ndtm) is defined similarly to a \tm, except
for $\delta$, which is defined as:
\[\fndef{\delta}{Q \times \Gamma}{\powerset{Q \times \Gamma \times \braces{L,R,S}}}.\]

The definitions of Multi-tape variants of both (deterministic) \tm\ and
non-deterministic \tm\ are similar to the original definition, again except for
the transition function, which is defined, respectively for each machine, as:
\[\fndef{\delta}{Q \times \Gamma^{k}}{Q \times \Gamma^{k} \times \braces{L,R,S}^{k}}\]
and
\[\fndef{\delta}{Q \times \Gamma}{\powerset{Q \times \Gamma^{k} \times
      \braces{L,R,S}^{k}}}.\]
Note that these machines have $k$ tapes and $k$ tape heads, allowing the
movement of the head of each tape to be independent.


\begin{theorem}
  \label{thr:efficient-universal-ndtm}
  There exists an \ndtm\ \undtm\ such that, for every $x, \alpha \in \bitstrings$,
  $\fn{\undtm}{x,\alpha} = \fn{\tmofstring{\alpha}}{x}$, where
  $\tmofstring{\alpha}$ is the \ndtm\ represented by $\alpha$.
  If $\tmofstring{\alpha}$ halts on input $x$ within $T$ steps, then
  $\fn{\undtm}{x,\alpha}$ halts within $C\cdot T$ steps, where $C$ is
  independent of $\length{x}$ and only depends on the size of
  $\tmofstring{\alpha}$'s alphabet, number of tapes and number of states.
\end{theorem}


\begin{definition}[$\CompClass{NTIME}$]
\label{def:ntime}
For every function $\fndef{T}{\nats}{\nats}$ and language $L \subseteq
\bitstrings$, we say that $L \in \fn{\CompClass{NTIME}}{\fn{T}{n}}$
if there is a constant $c > 0$ and a $\parens{c \cdot \fn{T}{n}}$-time
\Ndtm\ (\ndtm) $M$ \mst\ $\forall x \in \bitstrings$, $x \in L \Leftrightarrow \fn{M}{x} = 1$.
\end{definition}

\begin{theorem}
  \label{theorem:np-equals-poly-ntime}
  \[\ClassNP = \bigcup_{c > 0}\fn{\CompClass{NTIME}}{n^{c}}.\]
\end{theorem}


\begin{theorem}[Non-Deterministic Time Hierarchy Theorem]
  \label{thr:non-deterministic-time-hierarchy-theorem}
  For any two time-constructible functions $\fn{f}{n}$ and $\fn{g}{n}$ satisfying
  $\fn{f}{n+1} = \littleo{\fn{g}{n}}$, we have
  \[\fn{\CompClass{NTIME}}{\fn{f}{n}} \subsetneq \fn{\CompClass{NTIME}}{\fn{g}{n}}.\]
\end{theorem}
\begin{iproof}
  The proof uses a diagonization argument.
  In the following, the language we consider is a subset of the unary language, i.e. $L \subseteq \finitestrings{\braces{1}}$.
  
  The diagonalizing \ndtm\ $D$ defines infinitely many finite intervals
  of natural numbers:
  $\mathsf{int}_0 = \braces{u_0 = 1,\ldots,u_1}$, $\mathsf{int}_1 =
  \braces{u_1+1,\ldots,u_2}$, $\mathsf{int}_2 = \braces{u_2+1,\ldots,u_3}$, etc;
  for $i \in \nats$, on input $1^n$ such that $n \in \mathsf{int}_i$ (i.e. $n$ is
  in the $i$-th interval), $D$ emulates $\tmofstring{i}$.
  %
  These intervals are defined in such a way that for every $i \in \nats$,
  interval $\mathsf{int}_i = \braces{u_i+1,\ldots,u_{i+1}}$ is such that $u_{i+1}
  \gtrapprox 2^{u_i}$.
  %
  The interval is so large that $D$ can deterministically simulate
  $\fn{\tmofstring{i}}{1^{u_i + 1}}$ when given input $1^{u_{i+1}}$ in
  $\fn{f}{u_{i+1}}$ time steps, which means it can output the opposite of what
  $\fn{\tmofstring{i}}{1^{u_i + 1}}$ would output.
  %
  For all other inputs $1^{n}$ with $n \in \braces{u_{i}+1,\ldots,u_{i+1}-1}$, $D$
  outputs whatever $\tmofstring{i}$ outputs on input $1^{n+1}$, and so it can
  simulate it non-deterministically.
  %
  The contradiction then comes from the assumption that there is an \ndtm\ $M$
  that decides exactly the same language as $D$ but in $\fn{f}{n}$ time:
  %
  Letting $i \in \nats$ be whatever number corresponds to $M$ (i.e. $i$ is such
  that $\tmofstring{i} = M$) and considering the corresponding interval
  $\mathsf{int}_i = \braces{u_i+1,\ldots,u_{i+1}}$, note that on one hand, since
  by assumption $\tmofstring{i}$ decides the same language as $D$ we have
  $\forall l \in \mathsf{int}_i$ that $\fn{D}{1^l}=\fn{\tmofstring{i}}{1^l}$,
  and on the other hand, by the definition of $D$ we have $\forall l \in
  \braces{u_{i}+1,\ldots,u_{i+1}-1}$ that
  $\fn{D}{1^l}=\fn{\tmofstring{i}}{1^{l+1}}$.
  %
  This implies $\fn{\tmofstring{i}}{1^{u_i+1}} = \fn{\tmofstring{i}}{1^{u_i+2}}$, $\fn{\tmofstring{i}}{1^{u_i+2}} =
  \fn{\tmofstring{i}}{1^{u_i+3}}$, and so on, up to $\fn{\tmofstring{i}}{1^{u_{i+1}-1}} = \fn{\tmofstring{i}}{1^{u_{i+1}}}$, and
  so we have $\fn{\tmofstring{i}}{1^{u_i+1}} = \fn{\tmofstring{i}}{1^{u_{i+1}}}$.
  %
  This is a contradiction: by definition of $D$, $\fn{D}{1^{u_{i+1}}}$ outputs the
  opposite of $\fn{\tmofstring{i}}{1^{u_i+1}}$.

  In an actual proof, these intervals are defined by a function
  $\fndef{h}{\nats}{\nats}$ that grows very fast (fast enough to allow
  simulating the \ndtm\ deterministically on input $1^n$ in time $\fn{h}{n+1}$).
  %
  One way to define function $\fndef{h}{\nats}{\nats}$ is: $\fn{h}{1} = 2$;
  $\fn{h}{i+1} = 2^{{\fn{h}{i}}^{1.2}}$.
\end{iproof}


\begin{proposition}
  \label{prop:P-subseteq-NP-subseteq-EXP}
  $\ClassP \subseteq \ClassNP \subseteq \ClassEXP$.
\end{proposition}
\begin{iproof}
  \begin{description}
  \item[$\ClassP \subseteq \ClassNP$:]
    Consider any language $L \in \ClassP$, and let $M_L$ be a \tm\ deciding $L$
    in polytime.
    Taking $\fndef{p}{\nats}{\nats}$ as the $0$ polynomial (so the witness is an
    empty string) and $M$ as $M_L$ in \Cref{def:class-NP}, it follows $L \in
    \ClassNP$.
  \item[$\ClassNP \subseteq \ClassEXP$:]
    Consider any language $L \in \ClassNP$, and let $\fn{p}{\cdot}$ and
    $M_L$ be, respectively, the polynomial and the \tm\ as in
    \Cref{def:class-NP} (for language $L$).
    %
    It is easy to see that the \tm\ $M$ that enumerates all $u \in
    \strings{\bools}{\fn{p}{\length{x}}}$, and then runs $M_L$ on
    input $(x,u)$, and outputs $1$ if and only if for at least some $u$,
    $\fn{M_L}{x,u} = 1$ takes at most $2^{n^c}$ steps, implying $L
    \in \ClassEXP$.
  \end{description}
\end{iproof}





\begin{definition}[$\ClassNEXP$]
  \label{def:class-NEXP}
  \[\ClassNEXP = \bigcup_{c \geq 1}\CompClass{NTIME}\left(2^{n^{c}}\right).\]
\end{definition}

\begin{theorem}
  \label{thr:EXP-neq-NEXP-implies-P-neq-NP}
  $\ClassEXP \neq \ClassNEXP \Rightarrow \ClassP \neq \ClassNP$.
\end{theorem}
\begin{iproof}
  We prove $\ClassP = \ClassNP \Rightarrow \ClassEXP = \ClassNEXP$.

  Consider any language $L \in \fn{\CompClass{NTIME}}{2^{n^c}}$, and let
  $M_L$ be a \ndtm\ that decides $L$.
  Consider language $L_{pad} = \braces{\parens{x, 1^{2^{{\length{x}^c}}}} \mid x
    \in L}$.
  Then, there is an \ndtm\ ${M_L}'$ that decides $L_{pad}$ in poly
  (non-deterministic) steps: first ${M_L}'$ checks if the input is of the
  correct form, and if it is not, it simply rejects; if the input is in the
  right format, ${M_L}'$ runs $M_L$ on input $x$ and outputs whatever $M_L$
  outputs.
  %
  It is easy to see that ${M_L}'$ decides $L_{pad}$ if and only if $M_L$ decides
  $L$. Since $\ClassP = \ClassNP$ then there is a \tm\ $M$ that
  decides $L_{pad}$.
  %
  But then $L$ is in $\ClassEXP$ since the TM ${M}'$ that first pads its input
  $x$ with $1^{2^{{\lvert x \rvert}^c}}$ and then runs $M$ on
  $\left(x, 1^{2^{{\lvert x \rvert}^c}}\right)$ decides $L$ (and runs in
  exp-time).
\end{iproof}



\subsection{\textsf{NP} completeness}
\label{sub:np-completeness}

\subimport{./nondeterministic/}{np-completeness}



%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../../report"
%%% End:
