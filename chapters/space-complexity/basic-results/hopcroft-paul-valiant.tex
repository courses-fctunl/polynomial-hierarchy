\begin{theorem}[Hopcroft-Paul-Valiant '77]
  \label{thr:time-space-log}
  For any time constructible function \fndef{T}{\nats}{\nats}:
  \[\fn{\CompClass{DTIME}}{\fn{T}{n} \log \fn{T}{n}} \subseteq
    \fn{\CompClass{SPACE}}{\fn{T}{n}}.\]
\end{theorem}
\begin{iproof}
  We prove this for an arbitrary time constructible \fndef{T}{\nats}{\nats}.
  %
  However, note that it would suffice to consider only the function $\fn{T}{n} =
  n$, since we could use the \emph{padding} argument to show:
  %
  \[\fn{\CompClass{DTIME}}{n \log n} \subseteq \fn{\CompClass{SPACE}}{n} \Rightarrow
    \fn{\CompClass{DTIME}}{\fn{T}{n} \log \fn{T}{n}} \subseteq
    \fn{\CompClass{SPACE}}{\fn{T}{n}}.\]

  For simplicity, we actually prove a weaker statement:
  \[\fn{\CompClass{DTIME}}{\fn{T}{n} \log \fn{T}{n}} \subseteq
    \fn{\CompClass{SPACE}}{\fn{T}{n} \log^2 \fn{T}{n}}.\]
  %
  The main idea of the proof is considering any \tm\ $M$ that runs in time
  $\fn{T}{n} \log \fn{T}{n}$ and showing how to simulate it using space
  $\fn{T}{n} \log^2 \fn{T}{n}$.

  For the proof idea,
  see~\url{https://youtu.be/-HBqDL24Vb4?si=yL\_b2S7-o6D5qabc}.
\end{iproof}

\begin{remark}
  \begin{itemize}
  \item \Cref{thr:time-space-log} is ``very'' model-independent;
  \item For time constructible function $T$, note that Space Hierarchy Theorem
    implies
    \[\fn{\CompClass{SPACE}}{\fn{T}{n}} \subsetneq
      \fn{\CompClass{SPACE}}{\fn{T}{n} \log \fn{T}{n}}.\] 
  \end{itemize}
\end{remark}



\Cref{thr:time-space-log} was later used (with additional ideas, like
alternating turing machines) to prove:
%
\begin{theorem}[PPST '83]
  \label{thr:time-ntime-relation}
  \[\fn{\CompClass{DTIME}}{n} \subsetneq \fn{\CompClass{NTIME}}{n}.\]
\end{theorem}

\begin{theorem}[Santhanam '01]
  \label{thr:time-ntime-relation-improved}
  \[\fn{\CompClass{DTIME}}{n\sqrt{\log^* n}} \subsetneq
    \fn{\CompClass{NTIME}}{n\sqrt{\log^* n}}. \]
\end{theorem}

Known proofs of \Cref{thr:time-ntime-relation,thr:time-ntime-relation-improved}
seem to crucially rely on the multi-tape \tm\ model.



\begin{remark}
  \label{remark:time-ntime-relation}
  Consider two time constructible functions \fndef{f,g}{\nats}{\nats} with
  $\fn{f}{n} = \littleo{\fn{g}{n}}$.
  %
  The padding argument can only be used to prove statements in one direction:
  %
  \[\fn{\CompClass{NTIME}}{\fn{f}{n}} \subseteq \fn{\CompClass{DTIME}}{\fn{f}{n}} \Rightarrow
    \fn{\CompClass{NTIME}}{\fn{g}{n}} \subseteq \fn{\CompClass{DTIME}}{\fn{g}{n}},\]
  or, equivalently,
  \[\fn{\CompClass{NTIME}}{\fn{g}{n}} \nsubseteq \fn{\CompClass{DTIME}}{\fn{g}{n}} \Rightarrow
    \fn{\CompClass{NTIME}}{\fn{f}{n}} \nsubseteq \fn{\CompClass{DTIME}}{\fn{f}{n}},\]
  % 
  So, we cannot hope to use this argument to generalize
  \Cref{thr:time-ntime-relation,thr:time-ntime-relation-improved}.
  %
  Furthermore, note that even if one were to prove an analogous result for every
  polynomial, this would not imply $\ClassP \neq \ClassNP$: it could be that
  even though one cannot deterministically solve an \ClassNP-complete problem in
  the same polynomial time as non-deterministically, maybe one could still solve
  it in time squared the time it would take non-deterministically.
\end{remark}



%%% Local Variables:
%%% mode: plain-tex
%%% TeX-master: "../../report"
%%% End:
