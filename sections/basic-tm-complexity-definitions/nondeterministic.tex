\subsection{The \textsf{NP} class}
\label{subsec:np-class}

\begin{definition}[The class $\CompClass{NP}$]
  \label{def:class-NP}
  A language $L \subseteq \left \{0,1\right\}^{*}$ is in $\CompClass{NP}$ if there is a polynomial $p : \mathbb{N} \rightarrow \mathbb{N}$ and a (\Pt) \pt\ $M$ s.t. $\forall x \in \left \{0,1\right\}^{*}$, 
  \[x \in L \qquad \Leftrightarrow \qquad \exists u \in \left \{0,1\right\}^{p\left(\left | x \right |\right)} \, s.t. \, M\left(x,u\right) = 1.\]
  If $x \in L$ and $u \in \left \{0,1\right\}^{p\left(\left | x \right |\right)}$ satisfy $M\left(x,u\right) = 1$, then we call $u$ a \emph{certificate} (or \emph{witness}) for $x$ (w.r.t. language $L$ and machine $M$).
\end{definition}

Contrarily to $\CompClass{P}$, it is not known whether $\CompClass{NP}$ is closed under complementation.


The definition of many of the variants of the \Tm\ only differ on the transition function. For instance, a \Ndtm\ (\ndtm) is defined similarly to a \tm, except for $\delta$, which is defined as: \[\delta: Q \times \Gamma \rightarrow \mathcal{P} \left(Q \times \Gamma \times \left\{L,R,S\right\}\right).\]

The definitions of \MachineFont{Multi\text{-}tape\text{ }Turing\text{ }Machines} and \MachineFont{Non\text{-}deterministic\text{ }Multi\text{-}tape\text{ }Turing\text{ }Machines} are similar to the original definition, again except for the transition function, which is defined, respectively for each machine, as:
\[\delta: Q \times \Gamma^{k} \rightarrow Q \times \Gamma^{k} \times \left\{L,R,S\right\}^{k}\]
and
\[\delta: Q \times \Gamma \rightarrow \mathcal{P} \left(Q \times \Gamma^{k} \times \left\{L,R,S\right\}^{k}\right).\]
Note that these machines have $k$ tapes and $k$ tape heads, allowing the movement of the head of each tape to be independent.


\begin{theorem}
  \label{thr:efficient-universal-ndtm}
  There exists an \ndtm\ \undtm\ such that, for every $x, \alpha \in \{0,1\}^*$,
  $\undtm(x,\alpha) = M_\alpha(x)$,
  where $M_\alpha$ is the \ndtm\ represented by $\alpha$.
  If $M_\alpha$ halts on input $x$ within $T$ steps, then
  $\undtm(x,\alpha)$ halts within $C T$ steps, where $C$ is
  independent of $\lvert x \rvert$ and only depends on the size of $M_\alpha$'s
  alphabet, number of tapes and number of states.
\end{theorem}


\begin{definition}[The class $\CompClass{NTIME}$]
\label{def:ntime}
For every function $T: \mathbb{N} \to \mathbb{N}$ and language $L \subseteq
\{0,1\}^{*}$, we say that $L \in \CompClass{NTIME}\left(T\left(n\right)\right)$
if there is a constant $c > 0$ and a $\parens{c \cdot T\left(n\right)}$-time
\Ndtm\ (\ndtm) s.t. $\forall x \in \{0,1\}^{*}$, $x \in L \Leftrightarrow M\left(x\right) = 1$.
\end{definition}

\begin{theorem}
  \label{theorem:np-equals-poly-ntime}
  \[\ClassNP = \bigcup_{c \in \mathbb{N}}\CompClass{NTIME}\left(n^{c}\right).\]
\end{theorem}


\begin{theorem}[Non-Deterministic Time Hierarchy Theorem]
  \label{thr:non-deterministic-time-hierarchy-theorem}
  For any two time-constructible functions $f(n)$ and $g(n)$ satisfying
  $f(n + 1) = o(g(n))$, we have
  \[\CompClass{NTIME}\left(f(n)\right) \subsetneq \CompClass{NTIME}\left(g(n)\right).\]
\end{theorem}
\begin{iproof}
  The proof uses a diagonization argument.
  In the following, the language we consider is a subset of the unary language, i.e. $L \subseteq \braces{1}^*$.
  
  The diagonalizing \ndtm\ $D$ defines infinitely many finite intervals
  of natural numbers:
  $\mathsf{int}_0 = \braces{u_0 = 1,\ldots,u_1}$, $\mathsf{int}_1 =
  \braces{u_1+1,\ldots,u_2}$, $\mathsf{int}_2 = \braces{u_2+1,\ldots,u_3}$, etc;
  for $i \in \nats$, on input $1^n$ such that $n \in \mathsf{int}_i$ (i.e. $n$ is
  in the $i$-th interval), $D$ emulates $M_i$.
  %
  These intervals are defined in such a way that for every $i \in \nats$,
  interval $\mathsf{int}_i = \braces{u_i+1,\ldots,u_{i+1}}$ is such that $u_{i+1}
  \gtrapprox 2^{u_i}$.
  %
  The interval is so large that $D$ can deterministically simulate
  $M_i(1^{u_i + 1})$ when given input $1^{u_{i+1}}$ in $f(u_{i+1})$ time steps,
  which means it can output the opposite of what $M_i(1^{u_i + 1})$ would
  output.
  %
  For all other inputs $1^{n}$ with $n \in \braces{u_{i}+1,\ldots,u_{i+1}-1}$, $D$
  outputs whatever $M_i$ outputs on input $1^{n+1}$, and so it can simulate it
  non-deterministically.
  %
  The contradiction then comes from the assumption that there is an \ndtm\ $M$
  that decides exactly the same language as $D$ but in $f(n)$ time:
  %
  Letting $i \in \nats$ be whatever number corresponds to $M$ (i.e. $i$ is such
  that $M_i = M$) and considering the corresponding interval $\mathsf{int}_i =
  \braces{u_i+1,\ldots,u_{i+1}}$, note that on one hand, since by assumption $M_i$
  decides the same language as $D$ we have $\forall l \in \mathsf{int}_i$ that
  $D(1^l)=M_i(1^l)$, and on the other hand, by the definition of $D$ we have
  $\forall l \in \braces{u_{i}+1,\ldots,u_{i+1}-1}$ that $D(1^l)=M_i(1^{l+1})$.
  %
  This implies $M_i(1^{u_i+1}) = M_i(1^{u_i+2})$, $M_i(1^{u_i+2}) =
  M_i(1^{u_i+3})$, and so on, up to $M_i(1^{u_{i+1}-1}) = M_i(1^{u_{i+1}})$, and
  so we have $M_i(1^{u_i+1}) = M_i(1^{u_{i+1}})$.
  %
  This is a contradiction: by definition of $D$, $D(1^{u_{i+1}})$ outputs the
  opposite of $M_i(1^{u_i+1})$.

  In an actual proof, these intervals are defined by a function $h:\nats
  \rightarrow \nats$ that grows very fast (fast enough to allow simulating the
  \ndtm\ deterministically on input $1^n$ in time $h(n+1)$).
  %
  One way to define function $h:\nats \rightarrow \nats$ is: $h(1) = 2$;
  $h(i+1) = 2^{{h(i)}^{1.2}}$.
\end{iproof}


\begin{proposition}
  \label{prop:P-subseteq-NP-subseteq-EXP}
  $\ClassP \subseteq \ClassNP \subseteq \ClassEXP$.
\end{proposition}
\begin{iproof}
  \begin{description}
  \item[$\ClassP \subseteq \ClassNP$:]
    Consider any language $L \in \ClassP$, and let $M_L$ be a \tm\ deciding $L$ in polytime.
    Taking $p:\nats \rightarrow \nats$ as the $0$ polynomial (so the witness is an
    empty string) and $M$ as $M_L$ in \Cref{def:class-NP}, it follows $L \in
    \ClassNP$.
  \item[$\ClassNP \subseteq \ClassEXP$:]
    Consider any language $L \in \ClassNP$, and let $p(\cdot)$ and $M_L$ be,
    respectively, the polynomial and the \tm\ as in \Cref{def:class-NP} (for
    language $L$).
    %
    It is easy to see that the \tm\ $M$ that enumerates all $u \in
    \{0,1\}^{p(\lvert x \rvert)}$, and then runs $M_L$ on input $(x,u)$, and
    outputs $1$ if and only if for at least some $u$ $M_L(x,u) = 1$ takes at
    most $2^{n^c}$ steps, implying $L \in \ClassEXP$.
  \end{description}
\end{iproof}





\begin{definition}[The class $\ClassNEXP$]
  \label{def:class-NEXP}
  \[\ClassNEXP = \bigcup_{c \geq 1}\CompClass{NTIME}\left(2^{n^{c}}\right).\]
\end{definition}

\begin{theorem}
  \label{thr:EXP-neq-NEXP-implies-P-neq-NP}
  $\ClassEXP \neq \ClassNEXP \Rightarrow \ClassP \neq \ClassNP$.
\end{theorem}
\begin{iproof}
  We prove $\ClassP = \ClassNP \Rightarrow \ClassEXP = \ClassNEXP$.

  Consider any language $L \in \CompClass{NTIME}\left(2^{n^c}\right)$, and let $M_L$ be
  a \ndtm\ that decides $L$.
  Consider language $L_{pad} = \left \{\left(x, 1^{2^{{\lvert x
            \rvert}^c}}\right) \mid x \in L \right \}$.
  Then, there is an \ndtm\ ${M_L}'$ that decides $L_{pad}$ in poly
  (non-deterministic) steps: first ${M_L}'$ checks if the input is of the
  correct form, and if it is not, it simply rejects; if the input is in the
  right format, ${M_L}'$ runs $M_L$ on input $x$ and outputs whatever $M_L$
  outputs.
  %
  It is easy to see that ${M_L}'$ decides $L_{pad}$ if and only if $M_L$ decides
  $L$. Since $\ClassP = \ClassNP$ then there is a \tm\ $M$ that
  decides $L_{pad}$.
  %
  But then $L$ is in $\ClassEXP$ since the TM ${M}'$ that first pads its input
  $x$ with $1^{2^{{\lvert x \rvert}^c}}$ and then runs $M$ on
  $\left(x, 1^{2^{{\lvert x \rvert}^c}}\right)$ decides $L$ (and runs in
  exp-time).
\end{iproof}



\subsubsection{\textsf{NP} completeness}
\label{subsub:np-completeness}

\begin{definition}[Karp reducibility]
  \label{def:reducibility}
  We say that a language $L \subseteq \{0,1\}^{*}$ is polynomial Karp reducible to a language ${L}' \subseteq \{0,1\}^{*}$ (denoted $L \KarpRed {L}'$) if there is a polynomial-time computable function $f: \{0,1\}^{*} \to \{0,1\}^{*}$ s.t. $\forall x \in \{0,1\}^{*}$,
  \[x \in L \Leftrightarrow f\left(x\right) \in {L}'.\]
\end{definition}

\begin{theorem}[Transitivity of $\KarpRed$]
  If $L \KarpRed {L}'$ and ${L}' \KarpRed {L}''$ then $L \KarpRed {L}''$.
\end{theorem}


\begin{definition}[$\ClassNP$-hard languages]
  \label{def:np-hard}
  We say that a language ${L}'$ is $\ClassNP$-hard if $\forall L \in \ClassNP, L \KarpRed {L}'$.
\end{definition}

\begin{definition}[$\ClassNP$-complete languages]
  \label{def:np-complete}
  We say that a language ${L}'$ is $\ClassNP$-complete if ${L}'$ is $\ClassNP$-hard and ${L}' \in \ClassNP$.
\end{definition}




Let \[\Lang{SAT} = \left\{\varphi : \Pred{CNF}(\varphi) \wedge \exists \vec{x} \text{ s.t. } \varphi\left(\vec{x}\right) = true \right\},\]
and let \[\Lang{3SAT} = \left\{\varphi : \varphi \in \Lang{SAT} \wedge \text{every clause has at most 3 literals}\right\}.\]
\begin{proposition}
  \label{prop:SAT-to-3SAT}
  Every formula $F$ in \Pred{CNF} with $n$ variables and $m$ clauses has an
  equivalent formula ${F}'$ in \Pred{CNF} with at most $n.m$ variables and $n.m$
  clauses, where every clause has at most 3 literals.
\end{proposition}
\begin{iproof}
  For any disjunction of literals:
  $x_1 \vee x_2 \vee \ldots \vee x_n$ is equivalent to
  $(x_1 \vee x_2 \vee z_1) \wedge (\neg z_1 \vee x_3 \vee z_2) \wedge \ldots
  \wedge (\neg z_{n-2} \vee x_n)$.
\end{iproof}

\begin{theorem}[Cook-Levin theorem]
  \Lang{3SAT} is \ClassNP-complete.
\end{theorem}
\begin{iproof}
  The execution of the \tm\ $M$ from \Cref{def:class-NP} can be encoded as a
  conjunction of a sequence of \Pred{CNF} formulas (which itself then also is a
  \Pred{CNF} formula).
  %
  At a very high level, since computation is local one can encode as \Pred{CNF}
  formulas the starting state, each computation step, and whether $M$ would
  accept the input, in such a way that $M$ accepts the input if and only if
  there is a truth assignment to the variables that models the \Pred{CNF}.
\end{iproof}


Let $\Lang{INDSET} = \left\{ \left \langle \graph{G}, k  \right \rangle : \graph{G} \text{ has an independent set of size } k\right\}$, for a graph $\graph{G}$.
\begin{theorem}
  $\Lang{INDSET}$ is \ClassNP-complete.
\end{theorem}
The proof of this theorem proves that $\Lang{SAT} \KarpRed \Lang{INDSET}$, and
hence that $\Lang{INDSET}$ is \ClassNP-complete.



\subsection{Ladner's Theorem: Existence of \textsf{NP}-intermediate problems.}
\label{subsec:ladner-theorem-existence-np-intermediate-problems}

\begin{theorem}
  \label{thr:ladner-theorem}
  If $\ClassP \neq \ClassNP$ then there is a language $L \in \ClassNP \setminus
  \ClassP$ such that $L$ is not \ClassNP-complete.
\end{theorem}
\begin{iproof}
  For any function $H:\nats \rightarrow \nats$, let $\Lang{SAT}_H$ be the language
  containing all length-$n$ satisfiable formulae that are padded with $n^{H(n)}$
  $1$'s:
  \[\Lang{SAT}_H = \left \{ \psi 0 1^{n^{H(n)}} : \psi \in \Lang{SAT} \wedge n =
      \lvert \psi \rvert \right \}.\]

  Intuitively, the goal is to define $H$ such that it adds enough padding to
  ensure $\Lang{SAT}_H$ is not \ClassNP-hard, but yet not so much padding that
  makes $\Lang{SAT}_H$ too easy (i.e. $\Lang{SAT}_H \in \ClassP$).
  
  For the full proof, refer to~\cite[Theorem 3.3]{complexitybook}.
  %
  %Define function $H$ as:\\

  %``$H(n)$ is the smallest number $i < \log \log n$ such that for every $x \in
  %\{0,1\}^*$ with $\lvert x \rvert \leq \log n$, $M_i$ halts on input $x$ within
  %$i {\lvert x \rvert}^i$ steps and outputs $1$ iff $x \in SAT_H$. If there is
  %no such $i$, then $H(n) = \log \log n$.''\\

  %For this definition of $H$, one can show the following:
  %\begin{itemize}
  %\item $SAT_H \in \bm{NP}$: for every $x \in SAT_H$, there is a witness $w$ of
  %  length at most polynomial in $\lvert x \rvert$ that attest that $x \in
  %  SAT_H$;
  %\item $SAT_H \not\in \bm{P}$: this is proven by contradiction (where the
  %  contradiction is with the assumption that $\bm{P} \neq \bm{NP}$):
  %  one assumes that $SAT_H \in \bm{P}$, meaning there is a TM that decides
  %  $SAT_H$ in polynomial time, and then uses this TM to decide $SAT$ in time
  %  smaller than possible (given the assumption that $\bm{P} \neq \bm{NP}$);
  %\item $SAT_H$ is not $\bm{NP}$-hard: this is also proven by contradiction
  %  (where the contradiction is once again with the assumption that $\bm{P} \neq
  %  \bm{NP}$):
  %  one uses the machine given by $H(n)$?
  %\end{itemize}
  %\todo{To Write (to finish writing)}
\end{iproof}


\subsection{The \textsf{coNP} class}
\label{subsec:conp-class}

\begin{definition}[The class $\bm{coNP}$]
  \label{def:coNP}
  \[\bm{coNP} = \left\{\overline{L}: L \in \bm{NP}\right\}\]
\end{definition}

Note that $\bm{coNP}$ is not the complement of $\bm{NP}$.
In fact, \[\bm{P} \subseteq \bm{NP} \cap \bm{coNP}.\]

To emphasize this, we give an alternative definition of the $\bm{coNP}$ class.

\begin{definition}[Alternative definition of the class $\bm{coNP}$]
  \label{def:coNP-alternative}
  A language $L \subseteq \left \{0,1\right\}^{*}$ is in $\bm{coNP}$ if there is a polynomial $p : \mathbb{N} \rightarrow \mathbb{N}$ and a polynomial-time TM $M$ s.t. $\forall x \in \left \{0,1\right\}^{*}$, 
  \[x \in L \qquad \Leftrightarrow \qquad \forall u \in \left \{0,1\right\}^{p\left(\left | x \right |\right)} \text{ we have } M\left(x,u\right) = 1.\]
\end{definition}

Note that the only difference between this alternative definition of $\bm{coNP}$ and the definition of $\bm{NP}$ is the quantifier (for $\bm{coNP}$ it is $\forall$ while for $\bm{NP}$ it is $\exists$).

Let $TAUTOLOGY = \left\{\varphi : \varphi \text{ is in CNF and } \forall assig \, \varphi\left(assig\right) = true \right\}$.
\begin{theorem}
  \label{theorem:taut-coNO-complete}
  $TAUTOLOGY$ is $\bm{coNP}$-complete.
\end{theorem}

Let $EQ$-$DNF = \left\{ \left \langle \varphi, \psi  \right \rangle : \forall assig \, \varphi\left(assig\right) = \psi\left(assig\right)\right\}$, where $\varphi, \psi$ are boolean formulas in Disjunctive Normal Form (DNF).
\begin{theorem}
  \label{theorem:eq-dnf-coNP-complete}
  $EQ$-$DNF$ is $\bm{coNP}$-complete.
\end{theorem}



\subsection{On $\sbm{NP}$ vs $\sbm{coNP}$}
\label{subsec:np-vs-conp}
Although unknown, it is believed that $\bm{NP} \neq \bm{coNP}$.
The main reason for this belief is that it does not seem possible to have a certificate with polynomial length (and which can be verified in polynomial time) that may guarantee that some formula $\varphi$ is a tautology (\textit{i.e.} that $\varphi \in TAUTOLOGY$).

At an intuitive level, it seems that to check if $\varphi \in \bm{coNP}$, we have to try all possible variable assignments and only then can conclude that $\varphi \in TAUTOLOGY$.
Note that this is the exact opposite as verifying if a formula is satisfiable (in this case, if $\neg\varphi \in SAT$).


\subsection{Exponential Time Hypothesis}
\label{subsec:exp-time-hypothesis}

\begin{hypothesis}
  \label{hypothesis:exp-time-hypothesis}
  $\exists \delta > 0$ \mst\ $3SAT \not\in \bm{DTIME}\left (2^{\delta n} \right )$.
\end{hypothesis}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../../report"
%%% End:
