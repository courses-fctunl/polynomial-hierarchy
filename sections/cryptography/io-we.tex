Indistinguishability Obfuscation was introduced in the seminal paper by Barak et
al.~\cite{C:BGIRSVY01}.

\begin{definition}
  \label{def:io-circuits}
  A PPT algorithm \io\ is a indistinguishability obfuscator if:
  \begin{enumerate}
  \item for any circuit $C$ computing a function $\braces{0,1}^n \rightarrow
    \braces{0,1}$, any security parameter $\lambda \in \nats$, any circuit $C' \in
    \supp\parens{\io\parens{1^\lambda,C}}$ and any $x \in \braces{0,1}^n$, we have
    $C\parens{x} = \parens{\io\parens{1^\lambda,C}}\parens{x}$.
  \item for any two circuits $C^0$ and $C^1$ computing functions
    $\braces{0,1}^{n_0} \rightarrow \braces{0,1}$ and $\braces{0,1}^{n_1}
    \rightarrow \braces{0,1}$, respectively, and satisfying $n_0 = n_1$ and
    $\forall x \in \braces{0,1}^{n_0}: C^0\parens{x} = C^1\parens{x}$, no PPT
    distinguishes distributions $\braces{\io\parens{1^\lambda,C^0}}$ and
    $\braces{\io\parens{1^\lambda,C^1}}$ with non-negligible advantage in
    $\lambda$.
  \end{enumerate}
\end{definition}


Witness Encryption schemes were introduced by Garg et al.~\cite{STOC:GGSW13}. 

\begin{definition}
  \label{def:we-scheme}
  A Witness Encryption scheme for an \np\ language $L$ with corresponding
  witness relation $R_L$ is a pair of PPTs $\we = \parens{E,D}$
  satisfying:
  \begin{enumerate}
  \item for any $\lambda \in \nats$, any $x \in L$, any witness $w_x$ such that
    $\parens{x,w_x} \in R_L$, any $b \in \bools$ and any $c \in
    \supp\parens{E\parens{1^\lambda,x,b}}$, we have
    $D\parens{c,w_x} = b$.
  \item for any $x \not\in L$, no PPT distinguishes distributions
    $\braces{E\parens{1^\lambda,x,0}}$ and
    $\braces{E\parens{1^\lambda,x,1}}$ with non-negligible advantage in
    $\lambda$.
  \end{enumerate}
\end{definition}





\subsection{(Null-)\io\ implies \we}
\label{subsec:null-io-we}

Fix any \np\ language $L$ and let $R_L$ be some statement-witness relation for
$L$.
%
Let $\mathcal{C}^{R_L} = \braces{C^{R_L}_n}_{n \in \nats}$ be a uniform circuit
family such that for some polynomial $p:\nats \rightarrow \nats$, and for every $x
\in \braces{0,1}^*$:
\[x \in L \Leftrightarrow \exists w \in \braces{0,1}^{p\parens{\abs{x}}}:
  C^{R_L}_{\abs{x}}\parens{x,w} = 1.\]
%
(As one might note, the $\mathcal{C}^{R_L}$ circuit family is not defined for
all possible input lengths.)
%
Consider message space $\bools$; for bit $b \in \bools$ and $x \in
\braces{0,1}^*$, we define circuit $C^{b,x}_{p\parens{\abs{x}}}\parens{w}$,
computing a function $\braces{0,1}^{p\parens{\abs{x}}} \rightarrow \braces{0,1}$
as:
\[C^{b,x}_{\abs{x}}\parens{w} \coloneqq C^{R_L}_{\abs{x}}\parens{x,w} \wedge b.\]
% 

Consider an indistinguishability obfuscator \io.
We define our Witness Encryption scheme as follows:
\begin{itemize}
\item $E$: on input $\parens{1^\lambda,x,b}$, where $x \in \braces{0,1}^*$
  and $b \in \bools$, output $c \gets \io\parens{1^\lambda,C_{\abs{x}}^{b,x}}$;
\item $D$: on input $\parens{c, w}$, output $c\parens{w}$.  
\end{itemize}

In the following, we assume without loss of generality that for every $n,s \in
\nats$ there is a null-circuit $0_{n,s}$ that maps any input $x \in
\braces{0,1}^{n}$ to $0$ that has size $s$.

\begin{theorem}
  If PPT \io\ satisfies \Cref{def:io-circuits}, then the Witness Encryption
  scheme above satisfies \Cref{def:we-scheme}.
\end{theorem}
\begin{proof}
  Correctness follows from the correctness of \io\ and the definition of circuit
  family $\mathcal{C}^{R_L}$.

  We now prove the construction's security:
  For any bit $b$ consider some statement $x \not\in L$.
  This means $\forall u \in \braces{0,1}^{p\parens{\abs{x}}}$,
  $C^{R_L}_{\abs{x}}\parens{x,w} = 0$.
  It then follows by the definition of circuit $C^{b,x}_{\abs{x}}\parens{w}$
  that it outputs $0$ for any input $w$.
  Let $0_{\abs{x}}$ denote a null-circuit with $p\parens{\abs{x}}$
  input bits, i.e. a circuit that outputs $0$ regardless of its input.
  Since \io\ satisfies \Cref{def:io-circuits} and because $C^{b,x}_{\abs{x}}$
  computes the same function as $0_{\abs{x}}$ for $x \not\in L$, it follows that
  no PPT adversary distinguishes distributions
  $\braces{\io\parens{1^\lambda,C^{b,x}_{\abs{x}}}}$ and
  $\braces{\io\parens{1^\lambda,0_{\abs{x}}}}$ with non-negligible advantage in
  $\lambda$.
  To conclude the proof, note that distribution
  $\braces{\io\parens{1^\lambda,0_{\abs{x}}}}$ is now independent of bit $b$,
  and thus our Witness Encryption scheme is secure (\Cref{def:we-scheme}).
  \qed
\end{proof}




%%% Local Variables:
%%% mode: plain-tex
%%% TeX-master: "../../report"
%%% End:
