To build up the intuition for the polynomial hierarchy, we first begin by studying the class $\bm{\Sigma_{2}^{p}}$.

\subsection{The classes $\sbm{\Sigma_{2}^{p}}$ and $\sbm{\Pi_{2}^{p}}$}
\label{subsec:sigma-2-class}

\begin{definition}[The class $\bm{\Sigma_{2}^{p}}$]
  \label{def:sigma-2-class}
  The class $\bm{\Sigma_{2}^{p}}$ is the set of all languages $L$ for which there exists a polynomial-time TM $M$ and a polynomial q s.t. \[x \in L \Leftrightarrow \exists u \in \left\{0,1\right\}^{q\left(\left | x \right |\right)} \forall v \in \left\{0,1\right\}^{q\left(\left | x \right |\right)} M\left(x,u,v\right) = 1\]
  $\forall x \in \left\{0,1\right\}^{*}$.
\end{definition}

Recall \[INDSET = \left\{ \left \langle G, k  \right \rangle : G \text{ has an independent set of size } k\right\}.\]

Let \[EXACT-INDSET = \left\{ \left \langle G, k  \right \rangle : \text{The largest independent set in } G \text{ has size } k\right\}.\]

\begin{theorem}
  \label{theorem:exact-indset-in-sigma-2}
  $EXACT-INDSET \in \bm{\Sigma_{2}^{p}}$.
\end{theorem}


Recall \[EQ-DNF = \left\{ \left \langle \varphi, \psi  \right \rangle : \forall assig \, \varphi\left(assig\right) = \psi\left(assig\right)\right\},\] where $\varphi, \psi$ are boolean formulas in DNF ($EQ$-$DNF \in \bm{coNP}$).
  
Let \[MIN-EQ-DNF = \left\{ \left \langle \varphi, k  \right \rangle : \exists \psi \text{ s.t. } \left | \psi \right | \leq k \wedge \forall assig \, \varphi \left(assig\right) = \psi \left(assig\right)\right\},\] where $\varphi, \psi$ are boolean formulas in DNF.
\begin{theorem}
  $MIN-EQ-DNF$ is $\bm{\Sigma_{2}^{p}}$-complete.
\end{theorem}



\begin{definition}[The class $\bm{\Pi_{2}^{p}}$]
  \label{def:pi-2-class}
  The class $\bm{\Pi_{2}^{p}}$ is the set of all languages $L$ for which there exists a polynomial-time TM $M$ and a polynomial q s.t. \[x \in L \Leftrightarrow \forall u \in \left\{0,1\right\}^{q\left(\left | x \right |\right)} \exists v \in \left\{0,1\right\}^{q\left(\left | x \right |\right)} M\left(x,u,v\right) = 1 \]
  $\forall x \in \left\{0,1\right\}^{*}.$
\end{definition}


Let \[\overline{MIN-EQ-DNF} = \left\{ \left \langle \varphi, k  \right \rangle : \forall \psi \text{ s.t. } \left | \psi \right | \leq k \, \exists assig \, \varphi \left(assig\right) \neq \psi \left(assig\right)\right\},\] where $\varphi, \psi$ are boolean formulas in DNF.
\begin{theorem}
  \label{theorem:min-eq-dnf-is-pi-2-complete}
  $\overline{MIN\text{-}EQ\text{-}DNF}$ is $\bm{\Pi_{2}^{p}}$-complete.
\end{theorem}


\subsection{The definition of the Polynomial Hierarchy}
\label{subsec:definition-of-ph}
Now, we finally give the definition of the \emph{Polynomial Hierarchy}, which generalizes the definition of $\bm{NP}$, $\bm{coNP}$, $\bm{\Sigma_{2}^{p}}$ and $\bm{\Pi_{2}^{p}}$.

\begin{definition}[The Polynomial Hierarchy]
  \label{def:ph}
  For $i \geq 1$, a language $L$ is in $\bm{\Sigma_{i}^{p}}$ if there exists a TM $M$ and a polynomial q s.t.
  \begin{align*}
  x \in L \Leftrightarrow
    \exists u_{1} &\in \left \{0,1 \right\}^{q\left(\left | x \right |\right)}
    \forall u_{2} \in \left \{0,1 \right\}^{q\left(\left | x \right |\right)}
    \ldots
    Q_{i}   u_{i} \in \left \{0,1 \right\}^{q\left(\left | x \right |\right)}\\
    &M\left(x, u_{1}, u_{2}, \ldots, u_{i}\right) = 1
  \end{align*}
  where $Q_{i}$ is either $\forall$ (if $i$ is even) or $\exists$ (if $i$ is odd).

  The \textbf{Polynomial Hierarchy} ($\bm{PH}$) is defined as $\bm{PH} = \cup_{i \geq 1} \bm{\Sigma_{i}^{p}}$.
\end{definition}


\begin{definition}[$\bm{\Pi_{i}^{p}}$]
  \label{def:the-pi-i-class}
  $\bm{\Pi_{i}^{p}} = \bm{co\Sigma_{i}^{p}} = \left\{\overline{L}: L \in \bm{\Sigma_{i}^{p}}\right\}$
\end{definition}

\begin{remark}
  \label{remark:remarks-ph}
  \begin{itemize}
  \item Taking into account the definition of $\bm{NP}$, it is easy to see that $\bm{\Sigma_{1}^{p}} = \bm{NP}$.
  \item In the same way, considering the alternative definition of $\bm{coNP}$, we have $\bm{\Pi_{1}^{p}} = \bm{coNP}$.
  \item Moreover, note that $\bm{\Sigma_{i}^{p}} \subseteq \bm{\Pi_{i+1}^{p}} \subseteq \bm{\Sigma_{i+2}^{p}}  \subseteq \ldots$.
    \item Finally, note that $\bm{PH} = \cup_{i \geq 1} \bm{\Pi_{i}^{p}}$.
  \end{itemize}

\end{remark}



\begin{definition}
  \label{def:ph-collapse}
  We say that ``The Polynomial Hierarchy \textit{collapses}'' if $\exists i \text{ s.t. } \bm{\Sigma_{i}^{p}} = \bm{\Sigma_{i+1}^{p}}$.
\end{definition}

\begin{theorem}
  \label{theorem:collapse}
  \begin{enumerate}
  \item $\forall i \geq 1$, if $\bm{\Sigma_{i}^{p}} = \bm{\Pi_{i+1}^{p}}$, then $\bm{PH} = \bm{\Sigma_{i+1}^{p}}$ (\textit{i.e.} the hierarchy collapses to the i\textit{th} level).
    \item If $\bm{P} = \bm{NP}$ then $\bm{P} = \bm{PH}$.
  \end{enumerate}
\end{theorem}


\begin{theorem}
  \label{thr:ph-complete-collapse}
  If $\exists L \text{ s.t. } L$ is $\bm{PH}$-complete, then the hierarchy collapses.
\end{theorem}
\begin{proof}[Proof idea].\\
  \begin{enumerate}
  \item By definition $\bm{PH} = \cup_{i \geq 1} \bm{\Sigma_{i}^{p}}$.
  \item Since $L \in \bm{PH}$, $\exists i \text{ s.t. } L \in \bm{\Sigma_{i}^{p}}$.
  \item Since $L$ is $\bm{PH}$-complete, we can reduce all languages of $\bm{PH}$ to $L$.
  \item However, every language that is $\leq_{p}$ to a language in $\bm{\Sigma_{i}^{p}}$ is itself in $\bm{\Sigma_{i}^{p}}$, implying $\bm{PH} \subseteq \bm{\Sigma_{i}^{p}}$.
  \item Thus, $\bm{PH} = \bm{\Sigma_{i}^{p}}$
  \end{enumerate}
\end{proof}


\subsection{Complete problems for each level}
\label{subsec:complete-problems-for-each-level}

$\forall i \geq 1$, consider the following complete problem (a variant of $SAT$, using $i$ quantifiers):
\[\bm{\Sigma_{i}}SAT = \exists u_{1} \forall u_{2} \ldots Q_{i}u_{i} \varphi\left(u_{1},u_{2},\ldots,u_{i}\right) = 1,\] where $\varphi$ is a boolean formula, each $u_{i}$ a boolean vector, and $Q_{i}$ either $\forall$ or $\exists$, depending on whether (respectively) $i$ is even or odd.

\begin{theorem}
  $\bm{\Sigma_{i}}SAT$ is $\bm{\Sigma_{i}^{p}}$-complete. 
\end{theorem}


\begin{corollary}
  $\overline{\bm{\Sigma_{i}}SAT}$ is $\bm{\Pi_{i}^{p}}$-complete. 
\end{corollary}

Note that $\overline{\bm{\Sigma_{i}}SAT}$ is analogous to $TAUTOLOGY$.

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../report"
%%% End:
